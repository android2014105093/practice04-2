package com.example.sselab.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SubActivity extends AppCompatActivity {
    EditText edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);
        edit = (EditText) findViewById(R.id.editText);
    }

    public void onClickBtn2(View v) {
        Intent intent = new Intent(SubActivity.this, MainActivity.class);
        intent.putExtra("INPUT_TEXT", edit.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }
}
